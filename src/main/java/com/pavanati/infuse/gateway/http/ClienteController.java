package com.pavanati.infuse.gateway.http;

import com.pavanati.infuse.domain.model.Cliente;
import com.pavanati.infuse.gateway.mapper.ClienteMapper;
import com.pavanati.infuse.gateway.model.request.ClienteRequest;
import com.pavanati.infuse.gateway.model.response.ClienteResponse;
import com.pavanati.infuse.service.cliente.ClienteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/cliente")
@Tag(name = "Cliente", description = "API de cadastro de clientes")
public class ClienteController {


    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping
    @Operation(summary = "Cria um novo cliente")
    public ResponseEntity<ClienteResponse> criar(@RequestBody ClienteRequest body) {
        Cliente cliente = clienteService.criar(clienteMapper.toModel(body));
        return ResponseEntity.ok(clienteMapper.toResponse(cliente));
    }

    @GetMapping
    @Operation(summary = "Lista clientes cadastrados")
    public ResponseEntity<List<ClienteResponse>> listar() {
        List<Cliente> clientes = clienteService.listar();
        return ResponseEntity.ok(clientes.stream().map(pessoa -> clienteMapper.toResponse(pessoa)).collect(Collectors.toList()));
    }

    @GetMapping("{id}")
    @Operation(summary = "Busca cliente por identificador")
    public ResponseEntity<ClienteResponse> busca(@PathVariable("id") Long id) {
        Cliente cliente = clienteService.buscaPorId(id);
        return ResponseEntity.ok(clienteMapper.toResponse(cliente));
    }

    @PutMapping("{id}")
    @Operation(summary = "Atualiza cliente por identificador")
    public ResponseEntity<ClienteResponse> atualizar(@PathVariable("id") Long id, @RequestBody ClienteRequest body) {
        Cliente cliente = clienteService.atualizar(clienteMapper.toModel(body), id);
        return ResponseEntity.ok(clienteMapper.toResponse(cliente));
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Remove pessoa por identificador")
    public ResponseEntity<?> deletar(@PathVariable("id") Long id) {
        clienteService.deletarPorId(id);
        return ResponseEntity.noContent().build();
    }
}
