package com.pavanati.infuse.gateway.http;

import com.pavanati.infuse.domain.model.Pedido;
import com.pavanati.infuse.gateway.mapper.PedidoMapper;
import com.pavanati.infuse.gateway.model.request.PedidoRequest;
import com.pavanati.infuse.gateway.model.response.PedidoResponse;
import com.pavanati.infuse.service.pedido.PedidoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pedidos")
@Tag(name = "Projetos", description = "API de cadastro de projetos")
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private PedidoMapper pedidoMapper;

    @PostMapping(consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<PedidoResponse>> criar(@RequestBody List<PedidoRequest> request) {
        List<Pedido> pedidos = pedidoService.criar(request);
        return ResponseEntity.ok(pedidos.stream().map(servico -> pedidoMapper.toResponse(servico)).collect(Collectors.toList()));
    }

    @GetMapping
    @Operation(summary = "Lista pedidos cadastrados")
    public ResponseEntity<List<PedidoResponse>> listar(@RequestParam(value = "dataCadastro", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataCricao, @RequestParam(value = "code", required = false) String code) {
        List<Pedido> pedidos = pedidoService.listar(dataCricao, code);
        return ResponseEntity.ok(pedidos.stream().map(projeto -> pedidoMapper.toResponse(projeto)).collect(Collectors.toList()));
    }

    @GetMapping("{code}")
    @Operation(summary = "Busca projeto por identificador")
    public ResponseEntity<PedidoResponse> busca(@PathVariable("code") String id) {
        Pedido pedido = pedidoService.buscaPorId(id);
        return ResponseEntity.ok(pedidoMapper.toResponse(pedido));
    }


    @DeleteMapping("{code}")
    @Operation(summary = "Remove pedido por identificador")
    public ResponseEntity<?> deletar(@PathVariable("code") String code) {
        pedidoService.deletarPorId(code);
        return ResponseEntity.noContent().build();
    }
}
