package com.pavanati.infuse.gateway.mapper;

import com.pavanati.infuse.domain.model.Cliente;
import com.pavanati.infuse.gateway.model.request.ClienteRequest;
import com.pavanati.infuse.gateway.model.response.ClienteResponse;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface ClienteMapper {

    Cliente toModel(ClienteRequest request);

    ClienteResponse toResponse(Cliente model);
}
