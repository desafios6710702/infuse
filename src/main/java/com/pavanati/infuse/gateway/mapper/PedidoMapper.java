package com.pavanati.infuse.gateway.mapper;

import com.pavanati.infuse.domain.model.Pedido;
import com.pavanati.infuse.gateway.model.request.PedidoRequest;
import com.pavanati.infuse.gateway.model.response.PedidoResponse;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface PedidoMapper {

    Pedido toModel(PedidoRequest request);

    PedidoResponse toResponse(Pedido model);
}
