package com.pavanati.infuse.gateway.model.request;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

import java.time.LocalDate;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "pedido")
public class PedidoRequest {

    private String code;

    private String nome;

    @Builder.Default
    private LocalDate dataCadastro = LocalDate.now();

    @Builder.Default
    private Double valorUnitario = 0D;

    @Builder.Default
    private Long quantidade = 1L;

    private Long clienteId;

}
