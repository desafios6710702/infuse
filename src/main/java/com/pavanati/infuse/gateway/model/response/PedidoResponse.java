package com.pavanati.infuse.gateway.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PedidoResponse {

    private String code;

    private String nome;

    private LocalDate dataCadastro;

    private Double valorUnitario;
    private Double valorTotal;

    private Long quantidade;

    private ClienteResponse cliente;

}
