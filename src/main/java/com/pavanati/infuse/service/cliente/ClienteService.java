package com.pavanati.infuse.service.cliente;

import com.pavanati.infuse.domain.model.Cliente;
import com.pavanati.infuse.domain.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criar(Cliente body) {
        return clienteRepository.save(body);
    }

    public Cliente atualizar(Cliente body, Long id) {
        buscaPorId(id);
        body.setId(id);
        return clienteRepository.save(body);
    }

    public List<Cliente> listar() {
        return clienteRepository.findAll();
    }

    public Cliente buscaPorId(Long id) {
        return clienteRepository.findById(id).orElseThrow(() -> new RuntimeException("Cliente não encontrado"));
    }

    public void deletarPorId(Long id) {
        clienteRepository.deleteById(id);
    }


}
