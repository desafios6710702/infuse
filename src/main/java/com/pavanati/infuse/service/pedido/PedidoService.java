package com.pavanati.infuse.service.pedido;

import com.pavanati.infuse.domain.model.Cliente;
import com.pavanati.infuse.domain.model.Pedido;
import com.pavanati.infuse.domain.model.QPedido;
import com.pavanati.infuse.domain.repository.PedidoRepository;
import com.pavanati.infuse.gateway.mapper.PedidoMapper;
import com.pavanati.infuse.gateway.model.request.PedidoRequest;
import com.pavanati.infuse.service.cliente.ClienteService;
import com.querydsl.core.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private PedidoMapper pedidoMapper;

    @Autowired
    private ClienteService clienteService;

    public List<Pedido> criar(List<PedidoRequest> request) {
        if (request.size() > 10) {
            throw new RuntimeException("Maximo de 10 pedidos por chamada");
        }

        return request.stream().map(pedidoRequest -> {
            Cliente cliente = clienteService.buscaPorId(pedidoRequest.getClienteId());
            Pedido pedido = pedidoMapper.toModel(pedidoRequest);
            pedido.setValorUnitario(aplicaDesconto(pedido.getValorUnitario(), pedido.getQuantidade()));
            pedido.setValorTotal(pedido.getValorUnitario() * pedido.getQuantidade());
            pedido.setCliente(cliente);
            return pedidoRepository.save(pedido);
        }).collect(Collectors.toList());
    }

    private Double aplicaDesconto(Double valor, Long quantidade) {
        if (quantidade >= 10) {
            valor = (valor / 100) * (90);
        } else if (quantidade >= 5) {
            valor = (valor / 100) * (100 - quantidade);
        }
        return valor;
    }

    public List<Pedido> listar(LocalDate dataCadastro, String code) {
        BooleanBuilder expression = new BooleanBuilder();
        if (Objects.nonNull(dataCadastro)) {
            expression = expression.and(QPedido.pedido.dataCadastro.eq(dataCadastro));
        }
        if (Objects.nonNull(code)) {
            expression = expression.and(QPedido.pedido.code.eq(code));
        }
        return StreamSupport.stream(pedidoRepository.findAll(expression).spliterator(), false).collect(Collectors.toList());
    }

    public Pedido buscaPorId(String code) {
        return pedidoRepository.findOne(QPedido.pedido.code.eq(code)).orElseThrow(() -> new RuntimeException("Pedido não encontrado"));
    }

    public void deletarPorId(String id) {
        Pedido pedido = buscaPorId(id);
        pedidoRepository.delete(pedido);
    }

}
