CREATE TABLE pedido (
id BIGINT  NOT NULL AUTO_INCREMENT,
code VARCHAR(50) NOT NULL UNIQUE,
nome VARCHAR(200) NOT NULL,
data_cadastro DATE NOT NULL,
valor_unitario FLOAT ,
valor_total FLOAT ,
quantidade BIGINT NOT NULL ,
id_cliente BIGINT  NOT NULL,
CONSTRAINT pk_servico PRIMARY KEY (id),
CONSTRAINT fk_cliente FOREIGN KEY (id_cliente)
REFERENCES cliente (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION)
